package com.itla.testappdb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.itla.testappdb.R;
import com.itla.testappdb.entity.Student;

import java.util.List;

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.ViewHolder> {

    private List<Student> students;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvName;
        public TextView tvMatricula;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvNameItem);
            tvMatricula = itemView.findViewById(R.id.tvMatriculaItem);
        }
    }

    public StudentsAdapter(List<Student> students){
        this.students = students;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View studentView = inflater.inflate(R.layout.student_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(studentView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(students.get(position).getName());
        holder.tvMatricula.setText(students.get(position).getRegistration());
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

}
