package com.itla.testappdb.exception;

public class AddStudentException extends Exception {

    private long code;

    public AddStudentException(long code) {
        this.code = code;
    }

    @Override
    public String getMessage() {

        String message = "";

        if(code <= 0){
            message = "Error al crear el estudiante";
        }

        return message;
    }
}
