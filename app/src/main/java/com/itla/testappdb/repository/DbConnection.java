package com.itla.testappdb.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbConnection extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String DB_NAME = "escuela.db";

    public DbConnection(@Nullable Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE \"estudiante\" (\"id\"\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\t\"nombre\"\tTEXT NOT NULL,\n" +
                "\t\"matricula\"\tTEXT NOT NULL\n" +
                ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
