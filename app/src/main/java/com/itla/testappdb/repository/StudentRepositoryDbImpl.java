package com.itla.testappdb.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.itla.testappdb.entity.Student;
import com.itla.testappdb.exception.AddStudentException;

import java.util.ArrayList;
import java.util.List;

public class StudentRepositoryDbImpl implements StudentRepository {

    private static final String TABLE = "estudiante";

    private DbConnection dbConnection;

    public StudentRepositoryDbImpl(Context context) {
        this.dbConnection = new DbConnection(context);
    }

    @Override
    public void create(Student student) {

        ContentValues cv = new ContentValues();
        cv.put("nombre", student.getName());
        cv.put("matricula", student.getRegistration());

        SQLiteDatabase db = dbConnection.getWritableDatabase();

        long id = 0;

        try {
            id = db.insert(TABLE, null, cv);

            if(id <= 0)
            throw new AddStudentException(id);

        }catch (AddStudentException e){
            Log.i("EstudianteRepositorio", e.getMessage());
        }finally {
            if(id > 0)
                Log.i("EstudianteRepositorio", "El estudiante se ha creado exitosamente: " + id);
        }

    }

    @Override
    public void update(Student student) {}

    @Override
    public void delete(Student student) {}

    @Override
    public Student find(int id) {
        return null;
    }

    @Override
    public List<Student> findAll() {

        List<Student> students = new ArrayList<>();

        Cursor cursor = dbConnection.getReadableDatabase()
                .rawQuery("SELECT * FROM " + TABLE, null);

        while(cursor.moveToNext()){
            Student student = new Student();
            student.setName(cursor.getString(cursor.getColumnIndex("nombre")));
            student.setRegistration(cursor.getString(cursor.getColumnIndex("matricula")));
            students.add(student);
        }

        cursor.close();

        return students;
    }
}
