package com.itla.testappdb.repository;

import com.itla.testappdb.entity.Student;

import java.util.List;

public interface StudentRepository {

    void create(Student student);

    void update(Student student);

    void delete(Student student);

    Student find(int id);

    List<Student> findAll();

}
