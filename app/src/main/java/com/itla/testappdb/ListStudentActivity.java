package com.itla.testappdb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.itla.testappdb.adapter.StudentsAdapter;
import com.itla.testappdb.repository.StudentRepository;
import com.itla.testappdb.repository.StudentRepositoryDbImpl;

public class ListStudentActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    StudentRepository studentRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_student);

        studentRepository = new StudentRepositoryDbImpl(this.getBaseContext());

        recyclerView = findViewById(R.id.rvStudents);

        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        adapter = new StudentsAdapter(studentRepository.findAll());

        recyclerView.setAdapter(adapter);

    }
}
