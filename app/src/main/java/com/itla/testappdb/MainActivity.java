package com.itla.testappdb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;
import com.itla.testappdb.entity.Student;
import com.itla.testappdb.repository.StudentRepository;
import com.itla.testappdb.repository.StudentRepositoryDbImpl;

public class MainActivity extends AppCompatActivity {

    StudentRepository studentRepository;

    private EditText etName;
    private EditText etRegistration;

    private Button btnSave;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        studentRepository = new StudentRepositoryDbImpl(this.getBaseContext());

        etName = findViewById(R.id.etName);
        etRegistration = findViewById(R.id.etRegistration);

        btnSave = findViewById(R.id.btnSave);
        btnCancel = findViewById(R.id.btnCancel);

        btnSave.setOnClickListener(v -> {
            if(saveStudent()){
                Snackbar.make( v, "Se ha guardado el estudiante", Snackbar.LENGTH_SHORT).show();
            }
        });

        btnCancel.setOnClickListener(v -> {
            Intent intent = new Intent(this, ListStudentActivity.class);
            startActivity(intent);
        });

    }

    private Boolean saveStudent() {
        Student student = new Student();

        student.setName(etName.getText().toString());
        student.setRegistration(etRegistration.getText().toString());

        studentRepository.create(student);

        etName.setText("");
        etRegistration.setText("");

        return true;
    }


}
